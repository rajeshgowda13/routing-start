import { Directive, OnInit, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  constructor(
    private elfRef: ElementRef, private renderer: Renderer2
  ) { }
  ngOnInit() {
    this.renderer.setStyle(this.elfRef.nativeElement, 'background-color', 'blue');
  }

}
