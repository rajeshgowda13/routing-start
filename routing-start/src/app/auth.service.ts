
export class AuthService {
    loggiedIn = false;

    isAuthenticated() {
        const promise = new Promise(
            (resolve, reject) => {
                setTimeout(() => {
                    resolve(this.loggiedIn)
                }, 800);
            }
        )
        return promise;
    }

    login() {
        this.loggiedIn = true;
    }
    logout() {
        this.loggiedIn = false;
    }
}